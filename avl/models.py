from django.db import models
from django.contrib.auth.models import AbstractUser


################## Administración de usuarios ########################
class Empresa (models.Model):
	nombre = models.CharField(max_length=30, blank=False)
	cuit = models.CharField(max_length=20, blank=True)
	direccion = models.CharField(max_length=50, blank=True)
	telefono = models.CharField(max_length=20, blank=True)
	
	def __str__(self):
		return u'%s' % (self.nombre)

class Grupo (models.Model):
	nombre = models.CharField(max_length=30, blank=False)
	padre = models.ForeignKey('self',null=True,blank=True)
	moviles = models.ManyToManyField('Movil')
	empresapadre = models.ForeignKey(Empresa)
	
	def __str__ (self):
		return self.nombre

class Usuario (AbstractUser):
	cliente = models.ForeignKey(Empresa,null=True, blank=True)
	tiemposesion = models.IntegerField(null=True, blank=True, default=120) #Tiempo de session valida en minutos
	#	def __init__(self, *args, **kwargs):
	#	self._meta.get_field('username').help_text='Requerido. 30 caracteres o menos. Letras, números y @/./+/-/_ solamente.'
	#	self._meta.get_field('username').label = 'Nombre de usuario'
	#	super(infoadicional,self).__init__(*args, **kwargs)		

########################################################################
	

################# Ente a monitorear ####################################		
class Movil(models.Model):
	nombre = models.CharField(max_length=20,blank=False)
	activo = models.BooleanField(default=1)
	propietario = models.ForeignKey(Empresa)
	
	def __str__ (self):
			return u'%s' % (self.nombre)


################ Subclases de movil	####################################		
class Persona (Movil):
	nombres = models.CharField(max_length=50,blank=False)
	apellidos = models.CharField(max_length=50,blank=False)
	documento = models.IntegerField(null=True, blank=True)
	nacimiento = models.DateTimeField(blank=False)
	def __str__ (self):
		return u'%s %s' % (self.nombres,self.apellidos)

class Vehiculo (Movil):
	TIPO = (
		('A', 'Auto'),
		('C', 'Camión'),
		('M', 'Camioneta'),
		('O', 'Colectivo'),
		('B', 'Barco'),
		('V', 'Avión'),
	)
	tipoVehiculo = models.CharField(max_length=1, choices=TIPO,blank=False)
	
	
	dominio = models.CharField(max_length=15,blank=True)
	interno = models.IntegerField(null=True, blank=True)
	color = models.CharField(max_length=10, blank=True)
	marca = models.CharField(max_length=15, blank=True)
	modelo = models.CharField(max_length=20, blank=True)
	def __str__ (self):
		return u'%s - %s' % (self.nombre,self.dominio)		

class Objeto (Movil):
	descripción = models.CharField(max_length=50,blank=False)
	peso = models.IntegerField(null=True, blank=True)
	dimensiones = models.CharField(max_length=20,blank=True)
	identificador = models.CharField(max_length=20,blank=True)
	def __str__ (self):
		return u'%s - %s' % (self.nombre,self.identificador)
##################################################################


################### Eventos #######################################
class Evento (models.Model):
	nombre = models.CharField(max_length=30, blank=False)
	alarma = models.BooleanField(default=1)
	
	def __str__ (self):
		return self.nombre

class Script (models.Model):
	nombre=models.CharField(max_length=20,blank=True)
	version=models.IntegerField(blank=False)
	descripcion=models.CharField(max_length=50,blank=True)
	marca=models.CharField(max_length=20, blank=False)
	modelo=models.CharField(max_length=20, blank=False)

	def __str__ (self):
		return u'%s v%d' % (self.nombre,self.version)


class ConfigEquipo (models.Model):
	script = models.ForeignKey(Script,blank=False)
	eventoCrudo = models.CharField(max_length=10, blank=False)
	evento = models.ForeignKey(Evento,blank=False)
	def __str__ (self):
		return u'%s - %s' % (self.eventoCrudo,self.evento.nombre)
####################################################################



################## Hardware de seguimiento ##########################		
class Equipo (models.Model):
	numeroserie = models.CharField(max_length=20,blank=True)
	imei = models.DecimalField(max_digits=15,decimal_places=0, blank=True)
	marca = models.CharField(max_length=20, blank=False)
	modelo = models.CharField (max_length=20, blank=True)
	equipoid = models.CharField (max_length=20, blank=True)
	protocolo = models.IntegerField(blank=False)
	script = models.ForeignKey(Script,null=True,blank=True)
	propietario = models.ForeignKey(Empresa,blank=False)
	linea = models.ForeignKey('Linea',null=True,blank=True)
	movil = models.ForeignKey(Movil,null=True,blank=True)
	
	def __str__ (self):
		return u'%s - %s' % (self.modelo,self.equipoid)
	
class Linea (models.Model):
	telefono = models.IntegerField(null=True, blank=True)
	proveedor = models.ForeignKey(Empresa,blank=False)
	numerosim = models.DecimalField(max_digits=15,decimal_places=0,null=True, blank=True)
	
	def __str__ (self):
		return u'%s - %s' % (self.telefono,self.numerosim)

########################################################################


################## Puntos ##############################################
class Posicion (models.Model):
	movil = models.ForeignKey('Movil',null=True, blank=True)
	equipo = models.ForeignKey('Equipo',blank=False)
	fechahoragps = models.DateTimeField(null=True,blank=True)
	fechahoramsj = models.DateTimeField(blank=False)
	latitud = models.IntegerField(null=True,blank=True)
	longitud = models.IntegerField(null=True,blank=True)
	velocidad = models.IntegerField(null=True,blank=True)
	altitud = models.IntegerField(null=True,blank=True)
	curso = models.IntegerField(null=True,blank=True)
	odometro = models.IntegerField(null=True,blank=True)
	estadogps = models.IntegerField(null=True,blank=True)
	entradas = models.IntegerField(null=True, blank=True)
	salidas = models.IntegerField(null=True, blank=True)
	evento = models.ForeignKey(Evento,blank=False)

	def __str__ (self):
		return u'%s - %s' % (self.movil,self.fechahoragps)
	
class UltimaPosicion (models.Model):
	movil = models.OneToOneField('Movil',blank=False,primary_key=True)
	equipo = models.ForeignKey('Equipo',blank=False)
	fechahoragps = models.DateTimeField(null=True,blank=True)
	fechahoramsj = models.DateTimeField(blank=False)
	latitud = models.IntegerField(null=True,blank=True)
	longitud = models.IntegerField(null=True,blank=True)
	velocidad = models.IntegerField(null=True,blank=True)
	altitud = models.IntegerField(null=True,blank=True)
	curso = models.IntegerField(null=True,blank=True)
	odometro = models.IntegerField(null=True,blank=True)
	estadogps = models.IntegerField(null=True,blank=True)
	entradas = models.IntegerField(null=True, blank=True)
	salidas = models.IntegerField(null=True, blank=True)
	evento = models.ForeignKey(Evento,blank=False)
	
	def __str__ (self):
		return u'%s - %s' % (self.movil,self.fechahoragps)

class Temperatura (models.Model):
	posicion=models.OneToOneField('Posicion',blank=False,unique=True)
	temp1=models.IntegerField(null=True,blank=True)
	temp2=models.IntegerField(null=True,blank=True)
	temp3=models.IntegerField(null=True,blank=True)
	temp4=models.IntegerField(null=True,blank=True)

	def __str__ (self):
		return u'%s - %s|%s|%s|%s' % (self.posicion.movil,self.temp1,self.temp2,self.temp3,self.temp4)

######################################################################


###################### Geozona #######################################
class Geozona(models.Model):
	cliente = models.ForeignKey(Empresa,null=True, blank=True)
	nombre = models.CharField(max_length=30, blank=False)
	usuario = models.CharField(max_length=30, blank=True)
	descripcion = models.CharField(max_length=50, blank=True)

class PuntoGeozona(models.Model):
	id_geozona = models.IntegerField(null=False, blank=False)
	latitud = models.IntegerField(null=True,blank=True)
	longitud = models.IntegerField(null=True,blank=True)

######################################################################

#################### Variables ####################################

class ListaVariables(models.Model):
	nombre = models.CharField(max_length=50, blank=False)
	TIPO = (
		('A', 'Acumulada'),
		('I', 'Instantanea'),
		('C', 'Calculada'),
	)
	tipoVariable = models.CharField(max_length=1, choices=TIPO,blank=False)
	modelo = models.CharField(max_length=20)
	