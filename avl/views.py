from datetime import datetime, timedelta, timezone
import json

from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User

from avl.forms import SignUpForm
from avl.models import UltimaPosicion, Posicion, Movil, Empresa, Temperatura, ListaVariables
from avl.models import Usuario


 ##########Gestion de usuario y login ############################################
def signup(request):
	if request.method == 'POST':  # If the form has been submitted...
		form = SignUpForm(request.POST)  # A form bound to the POST data
		if form.is_valid():  # All validation rules pass
		# Process the data in form.cleaned_data
			username = form.cleaned_data["username"]
			password = form.cleaned_data["password2"]
			email = form.cleaned_data["email"]
			cliente = form.cleaned_data["cliente"]
			first_name = form.cleaned_data["first_name"]
			last_name = form.cleaned_data["last_name"]
			# At this point, user is a User object that has already been saved
			# to the database. You can continue to change its attributes
			# if you want to change other fields.
			user = Usuario.objects.create_user(username, email, password, cliente=cliente)
			user.first_name = first_name
			user.last_name = last_name

			# Save new user attributes
			user.save()
		return HttpResponseRedirect('/avl')  # Redirect after POST
	else:
		form = SignUpForm()

	data = {'form': form}
	return render(request,'nuevousuario.html',data)

def ingresar(request):
	if not request.user.is_anonymous():
		return HttpResponseRedirect(request.GET.get('next', '/avl/pos'))
	if request.method == 'POST':
		formulario = AuthenticationForm(request.POST)
		if formulario.is_valid:
			usuario = request.POST['username']
			clave = request.POST['password']
			acceso = authenticate(username=usuario, password=clave)

			if acceso is not None:
				if acceso.is_active:
					login(request, acceso)
					return HttpResponseRedirect(request.GET.get('next', '/avl/pos'))
				else:
					return render(request,'noactivo.html')
			else:
				return render(request,'nousuario.html')
	else:
		formulario = AuthenticationForm()
	return render(request,'ingresar.html',{'formulario':formulario})

@login_required(login_url='/avl')
def privado(request):
	usuario = request.user
	return render(request,'privado.html', {'usuario':usuario})

@login_required(login_url='/avl')
def cerrar(request):
	logout(request)
	return HttpResponseRedirect('/avl')

##########################################################################################


@login_required(login_url='/avl')
def listadoPosiciones(request):

#Definir opciones para enviar al template
	o = {
		"isroot": 0,
		"usuario": request.user,
		"alertas": 3,
		"mensajes": 5,
	}

#Si el usuario es administrador obtener todos los moviles sino solo los del cliente
	if request.user.is_superuser == 1:
		m=Movil.objects.all()
		c=Empresa.objects.all()
		o["isroot"]=1
	else:
		m=Movil.objects.filter(propietario=request.user.cliente.id)
		c=request.user.cliente
		o["isroot"]=0

	ultpos = UltimaPosicion.objects.filter(movil__in=m)


	return render(request,'posicionActual.html',{'mov': m, 'cli':c, 'opt':o})


@login_required(login_url='/avl')
def listadoDatos(request):
	m=Movil.objects.all()
	v=ListaVariables.objects.all()

	return render(request,'reportededatos.html',{'mov':m, 'var':v})

def ajaxDatos(request):
	comando = json.loads(request.body.decode("utf-8"))
	m=list(Movil.objects.filter(id__in=comando['moviles']).order_by('nombre').values_list('id', flat=True))
	mnombre=list(Movil.objects.filter(id__in=comando['moviles']).order_by('nombre').values_list('id','nombre'))
	d=datetime.strptime(comando['desde'], '%d-%m-%Y %H:%M:%S')
	h=datetime.strptime(comando['hasta'], '%d-%m-%Y %H:%M:%S')

	print(comando['campos'])
	campos=list(ListaVariables.objects.filter(id__in=comando['campos']).values_list('modelo', flat=True))
	print(campos)


	p=Posicion.objects.filter(movil_id__in=m)\
		.filter(fechahoragps__range=(d,h))\
		.order_by('movil__nombre','-fechahoragps')\
		.values_list('movil_id',*campos)
	
	'''
	t=Temperatura.objects.filter(posicion__movil_id__in=m)\
		.filter(posicion__fechahoragps__range=(d,h))\
		.order_by('posicion__movil__nombre','-posicion__fechahoragps')\
		.values_list(posicion__movil_id, 'posicion__fechahoragps','posicion__evento__nombre','posicion__latitud','posicion__longitud','posicion__odometro','posicion__velocidad','temp1','temp2','temp3','temp4')
	'''

	p=list(p)
	salida=[]
	indice=0

	for movil in mnombre:
		movilactual=movil[0]
		arregloPuntos=[]
		x=0
		for x in range(indice,len(p)):
			if p[x][0]==movilactual:
				punto=[]
				for y in range(1,len(p[x])):
					punto.append(p[x][y])
				arregloPuntos.append(punto)
			else:
				break
		indice=x
		item={'idMovil': movil[0],'nombreMovil':movil[1],'puntos': arregloPuntos}
		salida.append(item)
		#print(salida)
	enco = lambda obj: (
		obj.timestamp()
		if isinstance(obj, datetime)
		else None
	)
	return HttpResponse(json.dumps(salida,default=enco), content_type="application/json")


def ajaxPuntos(request):
	comando = json.loads(request.body.decode("utf-8"))
	m=[]
	if 'cliente' in comando:
		if comando['cliente'] == 0:
			query=Movil.objects.all()
		else:
			query=Movil.objects.filter(propietario=comando['cliente'])
	
		for i in query:
			m.append(i.id)

	elif 'moviles' in comando:
		m=comando['moviles']
		

	if 'modo' not in comando:
		modo='diccArr'
	else:
		modo=comando['modo']


	if modo =='diccArr':
		salida = armarDiccionarioArregloPuntos(m,comando['campos'])
	elif modo == 'arrDicc':
		salida = armarArregloDiccionarioPuntos(m,comando['campos'])
	else:
		print('Modo desconocido:' + modo)

	return HttpResponse(json.dumps(salida), content_type="application/json")


def armarDiccionarioArregloPuntos(moviles,campos):
	ultimasPosiciones = UltimaPosicion.objects.filter(movil__in=moviles)


	salida={}
	for c in campos:
		salida[c]=[]

	for movil in moviles:
		for posicion in ultimasPosiciones:
			if posicion.movil.id == movil:
				if 'movil' in campos: 
					salida['movil'].append(movil)
				if 'latitud' in campos:
					salida['latitud'].append(posicion.latitud/100000)
				if 'longitud' in campos:
					salida['longitud'].append(posicion.longitud/100000)
				if 'altitud' in campos:
					try:
						salida['altitud'].append(posicion.altitud/10)
					except:
						salida['altitud'].append('Sin datos')
				if 'fechahoragps' in campos:
					salida['fechahoragps'].append(posicion.fechahoragps.replace(tzinfo=timezone.utc).timestamp())
				if 'velocidad' in campos:
					salida['velocidad'].append(posicion.velocidad/10)
				if 'curso' in campos:
					salida['curso'].append(posicion.curso/10)
				if 'odometro' in campos:
					salida['odometro'].append(posicion.odometro/1000)
				if 'evento' in campos:
					salida['evento'].append(posicion.evento.__str__())
				if 'fechahoramsj' in campos:
					salida['fechahoramsj'].append(posicion.fechahoramsj.replace(tzinfo=timezone.utc).timestamp())
				if 'tiempodetenido' in campos:
					try:
						ultimaConVelocidad=Posicion.objects.filter(movil=movil,velocidad__gt=100).order_by('-fechahoragps')[0:1].get()
						salida['tiempodetenido'].append((posicion.fechahoragps-ultimaConVelocidad.fechahoragps).seconds)
					except:
						pass
				if 'ignicion' in campos:
					if posicion.entradas==None:
						salida['ignicion'].append(2)
					else: 
						salida['ignicion'].append(posicion.entradas & 1)
	return salida

def armarArregloDiccionarioPuntos(moviles,campos):
	ultimasPosiciones = UltimaPosicion.objects.filter(movil__in=moviles)
	print(campos)
	salida=[]
	for movil in moviles:
		for posicion in ultimasPosiciones:
			dicc={}
			if posicion.movil.id == movil:
				if 'movil' in campos: 
					dicc['movil'] = movil
				if 'latitud' in campos:
					dicc['latitud'] = posicion.latitud/100000
				if 'longitud' in campos:
					dicc['longitud'] = posicion.longitud/100000
				if 'altitud' in campos:
					try:
						dicc['altitud'] = posicion.altitud/10
					except:
						dicc['altitud'] = ('Sin datos')
				if 'fechahoragps' in campos:
					dicc['fechahoragps'] = posicion.fechahoragps.replace(tzinfo=timezone.utc).timestamp()
				if 'velocidad' in campos:
					dicc['velocidad'] = posicion.velocidad/10
				if 'curso' in campos:
					dicc['curso'] = posicion.curso/10
				if 'odometro' in campos:
					dicc['odometro'] = posicion.odometro/1000
				if 'evento' in campos:
					dicc['evento'] = posicion.evento.__str__()
				if 'fechahoramsj' in campos:
					dicc['fechahoramsj'] = posicion.fechahoramsj.replace(tzinfo=timezone.utc).timestamp()
				if 'tiempodetenido' in campos:
					try:
						ultimaConVelocidad=Posicion.objects.filter(movil=movil,velocidad__gt=100).order_by('-fechahoragps')[0:1].get()
						dicc['tiempodetenido'] = ((posicion.fechahoragps-ultimaConVelocidad.fechahoragps).seconds)
					except:
						pass
				if 'ignicion' in campos:
					if posicion.entradas==None:
						dicc['ignicion'] = 2
					else: 
						dicc['ignicion'] = (posicion.entradas & 1)

				salida.append(dicc)
	return salida