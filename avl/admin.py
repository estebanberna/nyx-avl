from django.contrib import admin

from avl.models import Empresa,Linea,Equipo,Movil,Vehiculo,Persona,Objeto,Evento,Posicion,UltimaPosicion,Grupo,ConfigEquipo,Script,Usuario

admin.site.register(Empresa)
admin.site.register(Linea)
admin.site.register(Equipo)
admin.site.register(Movil)
admin.site.register(Persona)
admin.site.register(Vehiculo)
admin.site.register(Objeto)
admin.site.register(Evento)
admin.site.register(Posicion)
admin.site.register(UltimaPosicion)
admin.site.register(Grupo)
admin.site.register(ConfigEquipo)
admin.site.register(Script)
admin.site.register(Usuario)
