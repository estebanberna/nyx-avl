from django.conf.urls import url
from . import views
from . import utiles

urlpatterns = [
    url(r'^pos/$',views.listadoPosiciones),
    url(r'^dat/$',views.listadoDatos),
    url(r'^puntos/$',views.ajaxPuntos),
    url(r'^datos/$',views.ajaxDatos),
    url(r'^iconoMovil/([0-9])-([0-9][0-9][0-9])-([0-9])\.svg$',utiles.icono),
    url(r'^nuevousuario/$',views.signup),
	url(r'^$',views.ingresar),
	url(r'^privado/$',views.privado),
	url(r'^cerrar/$', views.cerrar),
]