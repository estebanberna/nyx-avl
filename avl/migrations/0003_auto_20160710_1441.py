# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-07-10 14:41
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('avl', '0002_temperatura'),
    ]

    operations = [
        migrations.AlterField(
            model_name='temperatura',
            name='posicion',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='avl.Posicion'),
        ),
    ]
