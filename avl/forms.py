from django import forms
from django.db import models
from django.contrib.auth.models import User
from django.forms import ModelForm
#from django.contrib.auth.forms import UserCreationForm
from avl.models import Usuario
#from seguimiento.models import Geozona
#from seguimiento.models import PuntoGeozona
 
 
class SignUpForm(ModelForm):
    error_messages = {'password_mismatch': ("Los campos de contraseña no coinciden")}
    password1 = forms.CharField(label=("Contraseña"),widget=forms.PasswordInput)
    password2 = forms.CharField(label=("Confirmación de contraseña"), widget=forms.PasswordInput, help_text=("Vuelva a introducir la contraseña."))

    class Meta:
        model = Usuario
        fields = ['username','first_name', 'last_name', 'email', 'cliente']

        labels = {
            'username': 'Nombre de usuario',
        }
        help_texts = {
            'username': 'Requerido. 30 caracteres o menos. Letras, números y @/./+/-/_ solamente.',
        }

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def save(self, commit=True):
        user = super(SignUpForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user

'''
class geozonaForm(ModelForm):

    class Meta:
        model = Geozona
        fields = ['cliente','nombre','descripcion']
        labels = {
            'cliente': 'Cliente',
            'nombre': 'Nombre de la Geozona',
            'descripcion': 'Descripción',
        }

class stringlatlong(models.Model):
    
    latitud = models.CharField(max_length=900,null=True,blank=True)
    longitud = models.CharField(max_length=900,null=True,blank=True)

class puntoGeozonaForm(ModelForm):

    class Meta:
        model = stringlatlong
        fields = ['latitud','longitud']
'''